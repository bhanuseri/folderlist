package com.corp.folderlist.service;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import javax.xml.bind.annotation.*;

import com.corp.folderlist.dto.FileInfo;
import com.corp.folderlist.dto.DirInfo;
import com.corp.folderlist.exception.ResourceNotFoundException;
import com.corp.folderlist.util.FileUtil;

@Service
@XmlRootElement(name = "Artifacts")
public class FolderListServiceImpl implements IFolderListService {
	
	/*
	 * This method returns the Directory information with list of files and subfolder for the input String Directory Path name.
	 * 
	 */

	
	public Map<String, Object> listDirectory(File  dir) throws ResourceNotFoundException {

		if (!FileUtil.isDirectory(dir))
			throw new ResourceNotFoundException("Directory Path is invalid");
		File[] content = dir.listFiles();

		List<DirInfo> files = new LinkedList<>();
		List<Map<String, Object>> folders = new LinkedList<>();
		
		//looping for each folder OR file
		for (File f : content) {
			
			//creating a object which stores info about file , directory
			DirInfo directory = new DirInfo();
			directory.setName(f.getName());
			directory.setSize(FileUtil.getFileSizeKiloBytes(f));
			directory.setPath(f.getPath());
			//folder.add(directory);
			
			if (f.isDirectory()) {
			
				directory.setFlag(1);//set flag for directory				
				
				Map<String, Object> folder = new HashMap<String, Object>();
				folder.put("folder", directory);
				folders.add(folder);//write the directory info of the folder
				Map<String, Object> subList = listDirectory(f);//recursive function calling for each sud directory folder
				folders.add(subList);//add the folder to folders
				
			} else {
				directory.setFlag(0);//set flag for file
				files.add(directory);//write the file info
			}
		}

		Map<String, Object> result = new HashMap<>();
		result.put("folders", folders);//writing collection of folders to result
		result.put("files", files);//writing collection of files to result
		return result;
	}

	
	/*
	 * This method returns the File information for the input String File name.
	 * 
	 */
	
	public FileInfo getFileInfo(File file) throws ResourceNotFoundException {

		if (!FileUtil.isFile(file))
			throw new ResourceNotFoundException("File Name is invalid");
		FileInfo fileInfo = new FileInfo();

		fileInfo.setName(FileUtil.getFileName(file));
		fileInfo.setName(FileUtil.getFileName(file));
		fileInfo.setSize(FileUtil.getFileSizeKiloBytes(file));
		fileInfo.setType(FileUtil.getFileType(file));
		fileInfo.setPath(file.getPath());
		fileInfo.setAbspath(file.getAbsolutePath());
		fileInfo.setParent(file.getParent());

		return fileInfo;//return file info object

	}// end of getFileInfo()

}// end of class()
